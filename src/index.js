import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';
import Table from './components/Table'

ReactDOM.render(
  <React.StrictMode>
    <App />
    {/* <Table/> */}
  </React.StrictMode>,
  document.getElementById('root')
);


