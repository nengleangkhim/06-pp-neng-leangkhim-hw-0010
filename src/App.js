import logo from './logo.svg';
import './App.css';
import React, { Component } from 'react'
import Table from './components/Table'
import NavBar from './components/NavBar'
import { Container } from 'react-bootstrap';
import {
    BrowserRouter as Router,
    Switch,
    Route
  } from "react-router-dom";
export default class App extends Component {
    render() {
        return (
                <Router>            
                    <div>         
                    <NavBar/>
                        <Container>
                            <Switch>
                                <Route path="/author" component={Table}/>
                            </Switch>
                        </Container>
                    </div>
                </Router>
        )
    }
}
