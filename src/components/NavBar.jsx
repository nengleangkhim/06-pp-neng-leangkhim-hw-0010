import React from 'react'
import { Navbar, Brand, Link, Toggle, Collapse, Nav,NavDropdown, Dropdown, Divider, Item, Button, Form,FormControl, Container } from 'react-bootstrap';

export default function Table() {
    return (
        <div>
            <Navbar bg="light" expand="lg">
                <Container>
                    <Navbar.Brand href="#">AMS Management</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="mr-auto">
                        <Nav.Link href="#">Home</Nav.Link>
                        <Nav.Link href="#">Post</Nav.Link>
                        <Nav.Link href="#">User</Nav.Link>
                        <Nav.Link href="#">Category</Nav.Link>
                        <Nav.Link href="/author">Author</Nav.Link>
                        </Nav>
                        <Form inline>
                        <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                        <Button variant="outline-success">Search</Button>
                        </Form>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </div>
    )
}
